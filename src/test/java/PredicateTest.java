import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

/*Tem um método test ou são basicamente de uma função com um único parâmetro que retorna
um boolean e são geralmente geralmente usados em filtros de streams.
A interface contém também os métodos negate, and e or para composição.*/
public class PredicateTest {
    @Test
    public void testPredicate() {
        List<String> names = Arrays.asList("John", "Smith", "Samueal", "Catley", "Sie");
        Predicate<String> nameStartsWithS = str -> str.startsWith("S");
        names.stream().filter(nameStartsWithS).forEach(System.out::println);
    }

    @Test
    public void testPredicateAndComposition() {
        List<String> names = Arrays.asList("John", "Smith", "Samueal", "Catley", "Sie");
        Predicate<String> startPredicate = str -> str.startsWith("S");
        Predicate<String> lengthPredicate = str -> str.length() >= 5;
        names.stream().filter(startPredicate.and(lengthPredicate)).forEach(System.out::println);
    }

    @Test
    public void testNegate() {
        List<String> names = Arrays.asList("John", "Smith", "Samueal", "Catley", "Sie");
        Predicate<String> startPredicate = str -> str.startsWith("S");
        Predicate<String> lengthPredicate = str -> str.length() > 5;
        // Pode-se usar lengthPredicate.negate() ou startPredicate.negate() para negar apenas um dos predicates.
        names.stream().filter(startPredicate.and(lengthPredicate).negate()).forEach(System.out::println);
    }
}
