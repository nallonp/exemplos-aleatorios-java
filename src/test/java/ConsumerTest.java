import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Stream;

/* Consumer -> Tem um parâmetro e não retorna nada.
Aceita um objeto através do método "acept(T t)" e
pode encadear operações com o método "andThen()".*/
public class ConsumerTest {
    @Test
    public void whenNamesPresentConsumeAll() {
        Consumer<String> printConsumer = System.out::println;
        Stream<String> cities = Stream.of("Sydney", "Dhaka", "New York", "London");
        cities.forEach(printConsumer);
    }

    @Test
    public void whenNamesPresentUseBothConsumer() {
        List<String> cities = Arrays.asList("Sydney", "Dhaka", "New York", "London");

        Consumer<List<String>> upperCaseConsumer = list -> {
            for (int i = 0; i < list.size(); i++) {
                list.set(i, list.get(i).toUpperCase());
            }
        };
        Consumer<List<String>> printConsumer = list -> list.stream().forEach(System.out::println);
        // encadeia a execução de dois consumers e aceita uma lista.
        upperCaseConsumer.andThen(printConsumer).accept(cities);
    }

}
