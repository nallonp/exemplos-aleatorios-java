import org.junit.jupiter.api.Test;

import java.util.Optional;
import java.util.function.DoubleSupplier;
import java.util.function.Supplier;

/* Tem apenas um método "get" (e no caso de primitivos como IntSupplier um getAsInt() por exemplo)
É utilizada para fornecer resultados e são úteis quando é necessário adiar a execução de um bloco de
código.
 */
public class SupplierTest {
    @Test
    public void supplier() {
        Supplier<Double> doubleSupplier1 = () -> Math.random();
        DoubleSupplier doubleSupplier2 = Math::random;

        System.out.println(doubleSupplier1.get());
        System.out.println(doubleSupplier2.getAsDouble());
    }

    @Test
    public void supplierWithOptional() {
        Supplier<Double> doubleSupplier = () -> Math.random();
        Optional<Double> optionalDouble = Optional.empty();
        // execução adiada.
        System.out.println(optionalDouble.orElseGet(doubleSupplier));
    }
}
