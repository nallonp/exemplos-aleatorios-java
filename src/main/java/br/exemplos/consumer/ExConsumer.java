package br.exemplos.consumer;

import com.sun.istack.internal.NotNull;

import java.util.function.Consumer;

public class ExConsumer implements Consumer<String> {
    @Override
    public void accept(String s) {

    }

    @NotNull
    @Override
    public Consumer<String> andThen(@NotNull Consumer<? super String> after) {
        return Consumer.super.andThen(after);
    }
}
