package br.exemplos.predicate;

import com.sun.istack.internal.NotNull;

import java.util.function.Predicate;

public class ExPredicate implements Predicate<String> {
    @Override
    public boolean test(String s) {
        return false;
    }

    @NotNull
    @Override
    public Predicate<String> and(@NotNull Predicate<? super String> other) {
        return Predicate.super.and(other);
    }

    @NotNull
    @Override
    public Predicate<String> negate() {
        return Predicate.super.negate();
    }

    @NotNull
    @Override
    public Predicate<String> or(@NotNull Predicate<? super String> other) {
        return Predicate.super.or(other);
    }
}
