package br.exemplos.function;

import com.sun.istack.internal.NotNull;

import java.util.function.Function;

public class ExFunction implements Function<String, Integer> {

    @Override
    public Integer apply(String s) {
        return s.length();
    }

    @NotNull
    @Override
    public <V> Function<V, Integer> compose(@NotNull Function<? super V, ? extends String> before) {
        return Function.super.compose(before);
    }

    @NotNull
    @Override
    public <V> Function<String, V> andThen(@NotNull Function<? super Integer, ? extends V> after) {
        return Function.super.andThen(after);
    }
}
